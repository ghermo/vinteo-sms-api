<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Teepluss\Restable\Contracts\Restable;
use App\Encounter;
use App\Pcounter;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use DB;
use Carbon;
use Session;

class EncounterController extends ApiController {

    protected $rest;

    function __construct(Restable $rest)
    {
        $this->rest = $rest;
    }


    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        return $this->rest->listing(Pcounter::paginate())->render();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $encounter = new Encounter();

        $v = Validator::make($request->all(), [
            'data'       => ['required']
        ]);

        if ($v->fails())
        {
//            dd($v->errors()->all());
            return $this->rest->unprocess($v->errors()->all())->render();
        }

        $encounter->flag = 1;
        $data = $request->input('data');
        $encounter->timestamp = $data;
//        $data = '[{"s":"A","t":"13:58"},{"s":"A","t":"13:59"},{"s":"A","t":"13:59"},{"s":"A","t":"14:01"},{"s":"A","t":"13:58:286"},{"s":"B","t":"13:58:519"}]';
        $this->countPerson($data);

        $encounter->save();

        return $this->rest->created($encounter)->render();
	}

    protected function countPerson($data) {


//        dd(json_decode($data));

        $jsondata = json_decode($data);

        if ( is_null($jsondata) ) {
            dd('not valid json data');
        }

        $now = date('Y-m-d');

        $d = json_decode($data);

        $pcounter = new Pcounter();
        $pcounter->countab = $d->countAB;
        $pcounter->countba = $d->countBA;
        $pcounter->starttime = $d->date . ' ' . $d->startTime;
        $pcounter->endtime = $d->date . ' ' . $d->endTime;
        $pcounter->device_id = (property_exists($d, 'deviceid')) ? $d->deviceid : '';
        $pcounter->save();

        // dd(json_decode($data));

    }

    public function checkTimeString($time) {

        $regex = '/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/';
        if (preg_match($regex, $time)) {
            return true;
        } else {
            return false;
        }

    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Get the people count given the specific time
	 *
	 * @param  string Date
	 * @return Response
	 */
	public function getRealtimeCount()
	{
		$date = Carbon\Carbon::now();

		if(!Session::has('onLoadTime'))
		{
			Session::flash('onLoadTime', $date);
		}
		
		//	dd(Session::get('onLoadTime'));
		
		$data = DB::table('pcounters')
			->where('starttime', '<=', $date)
			->where('starttime', '>=', Session::get('onLoadTime'))
			->select(DB::raw('sum(countab) - sum(countba) as PeopleCount'))
			->get();

		if($data[0]->PeopleCount == null) $data[0]->PeopleCount = 0;
		$data['time'] = $date;

		return $data;	
	}

	public function getHistoricalData() {
		return Pcounter::orderBy('endtime', 'ASC')->get();		
	}

	public function getHistoricalDataFormatted() {
		// hourly
		$data = Pcounter::select('countab', 'endtime')
			->groupBy(DB::raw('day(endtime), hour(endtime)'))
			->orderBy('endtime', 'ASC')
			->get();
		$parsedData = array();
		$currentCount = 0;
    	foreach ($data as $k) {
    		// if ((intval($k->countab) - intval($k->countba)) != 0) {
    			$currentCount = intval($k->countab);
    			$t = ceil((strtotime($k->endtime))/3600)*3600*1000;
        		$parsedData[] = array($t,$currentCount);
        	// }
    	}
    	return $parsedData;
		// return Pcounter::orderBy('endtime', 'ASC')->get();		
	}
}
