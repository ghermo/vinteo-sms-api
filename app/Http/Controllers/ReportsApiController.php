<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Pcounter;
use Teepluss\Restable\Contracts\Restable;

class ReportsApiController extends ApiController {

	protected $rest;

    function __construct(Restable $rest)
    {
        $this->rest = $rest;
    }

    public function index() {


        $pcounters = Pcounter::where('state','A')
            ->groupBy(\DB::raw('HOUR(timestamp)'))
            ->select(array(\DB::raw('COUNT(id) as c, state, timestamp')))
            ->orderBy('timestamp')
            ->get();

        $result = [];

        foreach($pcounters as $pcounter) {
            $data = [];
            $data['x'] = strtotime($pcounter->timestamp)*1000;
            $data['y'] = intval($pcounter->c);
            $result[] = $data;
        }

//        dd((object) $result);

        // $result = json_encode($result);

        // dd($result);

//        return view('home');
        return json_encode($result);
        // return $this->rest->signle($result)->render();
    }

}
