<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ApiController extends Controller {

    protected $limit = 10;


    public function getLimit()
    {
        return $this->limit;
    }

}
