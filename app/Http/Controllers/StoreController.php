<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Teepluss\Restable\Contracts\Restable;
use App\Store;

class StoreController extends ApiController
{

    protected $rest;

    function __construct(Restable $rest)
    {
        $this->rest = $rest;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Store::get();
        // return $this->rest->listing(Store::paginate())->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $store = Store::create(Request::all());
        return $store;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $store = Store::find($id);

        if($store)
        {
            $store->store_chain_id = Request::get('store_chain_id');
            $store->name = Request::get('name');
            $store->location = Request::get('location');
        }

        $store->save();

        return $store;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Store::destroy($id);
    }
}
