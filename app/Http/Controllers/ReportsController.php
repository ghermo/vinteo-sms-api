<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Pcounter;

class ReportsController extends Controller {

    public function footfall() {

        $pcounters = Pcounter::where('state','A')
            ->groupBy(\DB::raw('HOUR(timestamp)'))
            ->select(array(\DB::raw('COUNT(id) as c, state, timestamp')))
            ->orderBy('timestamp')
            ->get();

        $result = [];

        foreach($pcounters as $pcounter) {
            $data = [];
            $data['x'] = $pcounter->timestamp;
            $data['y'] = $pcounter->c;
            $result[] = $data;
        }

//        dd((object) $result);

        $result = json_encode($result);

        \JavaScript::put([
            'pcounter' => $result
        ]);

//        return view('home');
        return \View::make('home');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
