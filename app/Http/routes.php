<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('/login', 'HomeController@login');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['prefix' => 'api/v1', 'middleware' => 'cors'], function() {

    Route::get('encounters', 'EncounterController@index');
    Route::get('footfall', 'ReportsApiController@index');
    Route::get('realtime', 'EncounterController@getRealtimeCount');
    Route::get('historicalData', 'EncounterController@getHistoricalData');
    Route::get('getHistoricalDataFormatted', 'EncounterController@getHistoricalDataFormatted');
    

    Route::post('encounters', 'EncounterController@store');
    Route::post('authenticate', 'AuthenticateController@authenticate');
    Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');

    Route::resource('authenticate', 'AuthenticateController', [
        'only' => [
           'index'
       ]
   ]);
    
    Route::resource('device', 'DeviceController',[
        'only' => [
            'index', 'store', 'update', 'destroy'
        ]
    ]);

    Route::resource('store', 'StoreController',[
        'only' => [
            'index', 'store', 'update', 'destroy'
        ]
    ]); 

    Route::resource('user', 'UserController',[
        'only' => [
            'index', 'store', 'update', 'destroy'
        ]
    ]);

});

Route::group(['prefix' => 'reports'], function() {
    Route::get('footfall', 'ReportsController@footfall');

});

Route::get('/show-autoloaders', function(){
    foreach(spl_autoload_functions() as $callback)
    {
        if(is_string($callback))
        {
            echo '- ',$callback,"\n<br>\n";
        }

        else if(is_array($callback))
        {
            if(is_object($callback[0]))
            {
                echo '- ',get_class($callback[0]);
            }
            elseif(is_string($callback[0]))
            {
                echo '- ',$callback[0];
            }
            echo '::',$callback[1],"\n<br>\n";
        }
        else
        {
            var_dump($callback);
        }
    }
});