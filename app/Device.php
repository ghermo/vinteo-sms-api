<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'device';

    protected $primaryKey = 'device_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['store_id', 'inside_state', 'outside_deploy'];
}
