<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        // $this->call(FootfallSeeder::class);
        $this->call(DeviceTableSeeder::class);
        $this->call(StoreTableSeeder::class);
        $this->call(PcounterSeeder::class);
        $this->call(UserTableSeeder::class);

        Model::reguard();
	}

}
