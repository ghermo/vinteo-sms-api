<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\Store;

class StoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
        
        Store::truncate();

        Store::create([
            'user_id' => 1,
            'name' => "de Jesus' Coffee Shop",
            'location' => $faker->city . ', ' . $faker->country
        ]);

        Store::create([
            'user_id' => 2,
            'name' => 'Greg\'s Restaurant',
            'location' => $faker->city . ', ' . $faker->country
        ]);
        
       Store::create([
            'user_id' => 3,
            'name' => 'Nev\'s Condominium',
            'location' => $faker->city . ', ' . $faker->country
        ]);
        foreach(range(1,30) as $index)
        {
            Store::create([
                'user_id' => $faker->randomDigitNotNull(),
                'name' => $faker->realText(100),
                'location' => $faker->city . ', ' . $faker->country
                // 'timestamps' => $faker->dateTimeBetween($startDate = '-3 months', $endDate = 'now')
            ]);
        }
    }
}
