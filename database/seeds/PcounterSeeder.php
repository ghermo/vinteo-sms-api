<?php

use Illuminate\Database\Seeder;

use App\Pcounter;

class PcounterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        Pcounter::truncate();

        Pcounter::create([
        	'countab' => 5,
        	'countba' => 2,
        	'starttime'=> '2015-07-23 16:44:24',
			'endtime'=> '2015-07-23 16:44:54',
			'device_id'=> ''
    	]);

    	Pcounter::create([
        	'countab' => 10,
        	'countba' => 2,
        	'starttime'=> '2015-07-23 16:44:54',
			'endtime'=> '2015-07-23 16:45:24',
			'device_id'=> ''
    	]);
    
    	Pcounter::create([
        	'countab' => 20,
        	'countba' => 4,
        	'starttime'=> '2015-07-23 16:45:24',
			'endtime'=> '2015-07-23 16:45:54',
			'device_id'=> ''
    	]);
    }
}
