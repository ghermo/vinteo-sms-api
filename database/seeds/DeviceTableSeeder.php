<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\Device;

class DeviceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        Device::truncate();

        Device::create([
            'device_id' => '21b8674309fd6d52',
            'store_id' => 1,
            'inside_state' => $faker->randomElement(['A','B']),
            'outside_deploy' => $faker->randomElement(['Y','N'])
        ]);

        Device::create([
            'device_id' => '7bea11e398770a50',
            'store_id' => 2,
            'inside_state' => $faker->randomElement(['A','B']),
            'outside_deploy' => $faker->randomElement(['Y','N'])
        ]);

        Device::create([
            'device_id' => '3043ff4944f4cc90',
            'store_id' => 3,
            'inside_state' => $faker->randomElement(['A','B']),
            'outside_deploy' => $faker->randomElement(['Y','N'])
        ]);
        foreach(range(1,30) as $index)
        {
            Device::create([
                'store_id' => $faker->randomDigitNotNull(),
                'inside_state' => $faker->randomElement(['A','B']),
                'outside_deploy' => $faker->randomElement(['Y','N'])
            ]);
        }
    }
}
