<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();

        User::truncate();

        User::create([
            'username' => 'gidj',
            'password' => Hash::make('Jesus'),
            'email' => 'ginodejesus02@gmail.com',
            'first_name' => 'Gino',
            'last_name' => 'de Jesus',
            'role' => 'admin'
        ]);

        User::create([
            'username' => 'greg',
            'password' => Hash::make('Hermo'),
            'email' => 'greg@gmail.com',
            'first_name' => 'Greg',
            'last_name' => 'hermo',
            'role' => 'admin'
        ]); 

        User::create([
        	'username' => 'nevs',
        	'password' => Hash::make('Custodio'),
        	'email' => 'nevs@gmail.com',
        	'first_name' => 'nevs',
        	'last_name' => 'Custodio',
            'role' => 'admin'
    	]);

    	foreach(range(1,30) as $index)
        {
            User::create([
	        	'username' => $faker->unique()->realText(10),
	        	'password' => Hash::make($faker->realText(10)),
                'email' => $faker->unique()->email,
                'first_name' => $faker->realText(10),
                'last_name' => $faker->realText(10),
	        	'role' => 'user'
	    	]);
        }
    }
}
