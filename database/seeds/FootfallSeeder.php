<?php
/**
 * Created by PhpStorm.
 * User: grg021
 * Date: 5/31/15
 * Time: 3:38 PM
 */

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class FootfallSeeder extends Seeder {

    public function run()
    {
        $faker = Faker::create();

        \App\Pcounter::truncate();

        foreach(range(1,300) as $index)
        {
            \App\Pcounter::create([
                'device_id' => $faker->randomDigitNotNull(),
                'state' => $faker->randomElement(['A','B']),
                'timestamp' => $faker->dateTimeBetween($startDate = '-3 months', $endDate = 'now')
            ]);
        }
    }

}